from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile

from airflow.models import DAG
from airflow.hooks.http_hook import HttpHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import PythonOperator

import os
import io
import json
import zipfile
import requests
import airflow


GDELT_HTTP_CONNECTION = os.getenv('GDELT_HTTP_CONNECTION', 'gdelt-http')
GDELT_PGSQL_CONNECTION = os.getenv('GDELT_PGSQL_CONNECTION', 'gdelt-pgsql')
INSERT_CMD = """INSERT into gdelt_events (url, day)
                VALUES (%s, %s);"""
args = {
    'owner': 'airflow',
    'start_date': datetime(2019,4,29,23,59)
}

dag = DAG(
    dag_id='gdelt_download_and_persist',
    default_args=args,
    schedule_interval='*/15 * * * *', # run it every 15 mins
)

db_exists = PostgresOperator(
    task_id = 'target_exists',
    sql='CREATE TABLE IF NOT EXISTS gdelt_events (id BIGSERIAL PRIMARY KEY, url VARCHAR(255), day DATE NOT NULL);',
    database=os.getenv('POSTGRES_DB_NAME', 'bitnami_airflow'),
    postgres_conn_id=GDELT_PGSQL_CONNECTION,
    dag=dag
)

def get_file_data(url):
    '''Go retrieve contents from URL and unzip contents in memory

    Arguments:
    url - string - URL of file to download - assumed to be a zip file based on GDELT data storage patterns
    '''
    resp = requests.get(url)
    with zipfile.ZipFile(io.BytesIO(resp.content)) as z:
        for i in z.infolist():
            with z.open(i) as f:
                yield i.filename, f.read()


# https://blog.gdeltproject.org/gdelt-2-0-our-global-world-in-realtime/
# Last 15 Minutes CSV Data File List – English - http://data.gdeltproject.org/gdeltv2/lastupdate.txt
def get_gdelt_data(**kwargs):
    '''Get list of current GDELT files from latest update file per documentation
    '''
    api_hook = HttpHook(http_conn_id=GDELT_HTTP_CONNECTION, method='GET')
    pg_hook = PostgresHook(postgres_conn_id=GDELT_PGSQL_CONNECTION, schema=os.getenv('POSTGRES_DB_NAME', 'bitnami_airflow'))
    resp = api_hook.run('/gdeltv2/lastupdate.txt')
    files = {}
    lines = resp.text.split('\n')
    url = lines[0].split(' ')[-1]
    with NamedTemporaryFile() as ntf:
        with open(ntf.name, 'w') as w:
            for fn, d in get_file_data(url):
                rows = d.decode('utf-8').split('\n')
                for row in [x.strip().split('\t') for x in rows if x.strip()]:
                    if row and len(row) > 0:
                        url = row[-1]
                        day = '{}-{}-{}'.format(row[1][4:6], row[1][6:8], row[3])
                        pg_hook.run(INSERT_CMD, parameters=(url, day))


get_data = PythonOperator(
    task_id='get_gdelt_data',
    provide_context=True,
    python_callable=get_gdelt_data,
    dag=dag
)

dag >> db_exists >> get_data

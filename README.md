# Apache Airflow w/ Bitnami and Azure

## Resources

* [Apache Airflow](https://airflow.apache.org)
    * [DAGs](https://airflow.apache.org/concepts.html#dags)
    * [Operators](https://airflow.apache.org/concepts.html#operators)
    * [How-to Guides](https://airflow.apache.org/howto/index.html)
* [Azure Marketplace - Apache Airflow Multi-Tier by Bitnami](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/bitnami.airflow-multitier?tab=Overview)
    * Bitnami Documentation - [Synchronize DAGs with a Remote Git Repository](https://docs.bitnami.com/azure-templates/infrastructure/apache-airflow/configuration/sync-dags/)
* [Demo code](https://gitlab.com/introspectdata/public/airflow-demo)
* [Awesome Airflow](https://github.com/jghoman/awesome-apache-airflow) - curated list of resources
    * [Lessons learned while airflow-ing](https://medium.com/@nehiljain/lessons-learnt-while-airflow-ing-32d3b7fc3fbf)

## Running Locally

This repository is set up to use [docker-compose](https://docs.docker.com/compose/overview/) to handle the process of running [Apache Airflow](https://airflow.apache.org/) in a consistent manner.

Special thanks to [Bitnami](https://bitnami.com) for hosting us at their webinar and for putting together an awesome [Multi-tier Apache Airflow](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/bitnami.airflow-multitier?tab=Overview) one-click deployment for Azure!

Want to take it for a spin? It's pretty easy:

* [ ] Write your DAG and put it in the root directory of this repository.
* [ ] Fire up docker-compose (`docker-compose up -d`)
* [ ] Go play with airflow via a web browser at [http://localhost:8080](http://localhost:8080)
